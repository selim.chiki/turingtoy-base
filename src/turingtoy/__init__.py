from typing import Dict, List, Optional, Tuple

def run_turing_machine(
    machine: Dict, input_: str, steps: Optional[int] = None
) -> Tuple[str, List, bool]:
    # Initialisation des variables
    step = 0
    head_position = 0
    blank_symbol = machine["blank"]
    initial_state = machine["start state"]
    final_state = machine["final states"]
    transition_table = machine["table"]
    states = machine["table"].keys()
    tape: list[str] = list(input_)
    execution_history = []
    current_state = initial_state
    
    # Boucle principale de la machine de Turing
    while current_state not in final_state:
        if steps is not None and step >= steps:
            break
        
        # Vérification et ajustement de la position de la tête de lecture
        if head_position < 0:
            tape.insert(0, blank_symbol)
            head_position = 0
        elif head_position >= len(tape):
            tape.append(blank_symbol)
        
        symbol = tape[head_position]
        
        # Vérifie si la transition est définie pour l'état et le symbole actuels
        if symbol not in transition_table[current_state]:
            break
        
        transition = transition_table[current_state][symbol]
        
        # Ajout de l'entrée à l'historique d'exécution
        history_dict = {
            'state': current_state,
            'reading': symbol,
            'position': head_position,
            'memory': ''.join(tape),
            'transition': transition
        }
        execution_history.append(history_dict)
        
        # Exécution de la transition
        if transition == "L":
            head_position -= 1
        elif transition == "R":
            head_position += 1
        else:
            if "write" in transition:
                tape[head_position] = transition['write']
            if "L" in transition:
                head_position -= 1
                current_state = transition['L']
            elif "R" in transition:
                head_position += 1
                current_state = transition['R']
            else:
                head_position += 0
                break
        
        step += 1
    
    # Obtention du résultat en supprimant les symboles blancs supplémentaires
    result = "".join(tape).strip(blank_symbol)
    return (result, execution_history, current_state in final_state)
